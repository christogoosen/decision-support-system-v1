<?php
class seantestController extends BaseController
{
	
	public static function fetchTestData()
	{
		$testData = DB::table('applicants')->get();
		//var_dump($testData);

		$chartArray["chart"] = array("type" => "column"); 
		$chartArray["title"] = array("text" => "3rd Year mark for each student"); 
		$chartArray["credits"] = array("enabled" => false); 
		$chartArray["navigation"] = array("buttonOptions" => array("align" => "right")); 
		$chartArray["series"] = array(); 
		$chartArray["xAxis"] = array("categories" => array()); 
		foreach ($testData as $student) 
		{ 
  			$categoryArray[] = $student->studentNumber; 
  			//don't show users who have no assets
  			$chartArray["series"][] = array("name" => $student->studentNumber, "data" => array((int)($student->thirdYearAverage))); 
  			
		} 
		$chartArray["xAxis"] = array("categories" => $categoryArray); 
		$chartArray["yAxis"] = array("title" => array("text" => "Total Assets")); 
	//var_dump($chartArray);
	return View::make('graphTest')->with("chartArray", $chartArray);

		//return $testData;
	}

	public static function testAlldata()
	{
		$allTestData = DB::table('applicants')->get();
		foreach ($allTestData as $key) {
			var_dump($key -> studentNumber);
		}
		//var_dump($allTestData);
	}
}

