<?php

class GraphController extends BaseController {

	public static function sgraphs($graph1Arr,$graph2Arr, $graph3Arr)
	{
		 $grap1JSON = GraphController::graph1();  
		 $grap1JSON = GraphController::graph2();
		 $grap1JSON = GraphController::graph3();
	}

	public static function graph1()
	{
		$testData = DatabaseController::graph3();
		$Above60Array = array();
		$Below60Array = array();

		foreach ($testData as $key ) 
		{
		$count =0;
		foreach ($key as $student) 
		{ 
  			//$categoryArray[] = $student[0]; 
  			
  			//don't show users who have no assets
  			if($count<1){

  			array_push($Above60Array,array(((float)$student[1]),((float)$student[2])));
  			}
  			else
  			{
  				array_push($Below60Array,array(((float)$student[1]),((float)$student[2])));
  			}//var_dump($chartArray["series"]);
  			//var_dump(json_encode(array(array((float)$student[1],(float)$student[2]))));
		} 
		//var_dump($chartArray["series"]["data"]);
		
		$count +=1;
  			if($count>0)
  			{
  				$seriesName="Below 60"; 
  			}

		}
		$chartArray = array($Above60Array, $Below60Array);

	return View::make('graphTest')->with(array("Above60Array"=> $Above60Array,"Below60Array"=>$Below60Array));
	

		//return $json;
	}

	public static function graph2()
	{
		return $json;
	}

	public static function graph3()
	{
		$testData = DatabaseController::graph3();
		$chartArray["chart"] = array("type" => "column"); 
		$chartArray["title"] = array("text" => "Module Averages over three years"); 
		$chartArray["credits"] = array("enabled" => false); 
		$chartArray["navigation"] = array("buttonOptions" => array("align" => "right")); 
		$chartArray["series"] = array(); 
		$chartArray["xAxis"] = array("categories" => array()); 
		foreach ($testData as $student) 
		{ 
  			$categoryArray[] = $student[0]; 
  			//don't show users who have no assets
  			$chartArray["series"][] = array("name" => $student[0], "data" => array((float)$student[1],(float)$student[2],(float)$student[3])); 
  			//var_dump($chartArray["series"]);
		} 
		//var_dump($testData[3]);
		$chartArray["xAxis"] = array("categories" => $categoryArray); 
		$chartArray["yAxis"] = array("title" => array("text" => "Total Assets")); 
	
	return View::make('graphTest')->with("chartArray", $chartArray);
		//$json = $chartArray;

		//return $json;
	}
}