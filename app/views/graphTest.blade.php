@extends('layouts.main')

@section('content')



<div class='alert alert-info'>
    {{Session::get('message')}}
</div>


<div id="container1" class="span5"></div>
<div id="container2" class="span6"></div>
<div id="container3" class="span7"></div>	


<script type="text/javascript">
$(function () {
    $('#contain').highcharts({
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: 'Height Versus Weight of 507 Individuals by Gender'
        },
        subtitle: {
            text: 'Source: Heinz  2003'
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Height (cm)'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: 'Weight (kg)'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x} cm, {point.y} kg'
                }
            }
        },
        series: [{
            name: 'Female',
            color: 'rgba(223, 83, 83, .5)',
            data: {{json_encode($Below60Array)}}}

        }, {
            name: 'Above 60',
            color: 'rgba(119, 152, 191, .5)',
            data: {{json_encode($Above60Array)}}}
        }]
    });
});
$(function() {
  $('#container3').highcharts(
    {{json_encode($Above60Array)}}
  )
});
$(function() {
  $('#container1').highcharts(
    {{json_encode($Below60Array)}}
  )
});
</script>



@stop
