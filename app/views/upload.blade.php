@extends('layouts.main')

@section('content')

{{FileController::wipeCache()}}
{{DatabaseController::clearDatabase()}}

@if(Session::has('message'))
<div class='alert alert-danger'>
	{{Session::get('message')}}
</div>
@endif

<div class='container-inner'>
{{ Form::open(array('url'=>'upload-zip','files'=>true)) }}
  
  {{ Form::label('file','Upload Your File:',array('id'=>'','class'=>'')) }}
  {{ Form::file('file','',array('id'=>'','class'=>'')) }}

<br>

  <!-- submit buttons -->
  {{ Form::submit('Save', ['class' => 'btn btn-info' ]) }}
  
  <!-- reset buttons -->
  {{ Form::reset('Reset', ['class' => 'btn btn-info' ]) }}

  {{ Form::close() }}

</div>

@stop