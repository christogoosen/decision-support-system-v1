<?php

class TestSeeder extends Seeder {

	public function run()
	{
		DB::table('applicants')->delete();
		Applicant::create(array(
			'studentNumber' => 16613600,
        	'101' => 57,
        	'102' => 63,
        	'103' => 64,
        	'firstYearAverage' => 65.00,
        	'201' => 54,
        	'202' => 64,
        	'203' => 62,
        	'secondYearAverage' => 67.00,
        	'301' => 54,
        	'302' => 62,
        	'303' => 64,
        	'thirdYearAverage' => 68.00));

		Applicant::create(array(
			'studentNumber' => 13958285,
        	'101' => 60,
        	'102' => 43,
        	'103' => 64,
        	'firstYearAverage' => 65.00,
        	'201' => 54,
        	'202' => 60,
        	'203' => 62,
        	'secondYearAverage' => 67.00,
        	'301' => 54,
        	'302' => 62,
        	'303' => 64,
        	'thirdYearAverage' => 68.00));

		Applicant::create(array(
			'studentNumber' => 16650816,
        	'101' => 74,
        	'102' => 88,
        	'103' => 56,
        	'firstYearAverage' => 65.00,
        	'201' => 76,
        	'202' => 65,
        	'203' => 62,
        	'secondYearAverage' => 67.00,
        	'301' => 56,
        	'302' => 62,
        	'303' => 64,
        	'thirdYearAverage' => 68.00));

		Applicant::create(array(
			'studentNumber' => 15555567,
        	'101' => 58,
        	'102' => 64,
        	'103' => 65,
        	'firstYearAverage' => 65.00,
        	'201' => 64,
        	'202' => 70,
        	'203' => 72,
        	'secondYearAverage' => 67.00,
        	'301' => 64,
        	'302' => 72,
        	'303' => 74,
        	'thirdYearAverage' => 68.00));
	

	    Applicant::create(array(
			'studentNumber' => 13456789,
        	'101' => 47,
        	'102' => 53,
        	'103' => 44,
        	'firstYearAverage' => 65.00,
        	'201' => 34,
        	'202' => 60,
        	'203' => 62,
        	'secondYearAverage' => 67.00,
        	'301' => 44,
        	'302' => 62,
        	'303' => 64,
        	'thirdYearAverage' => 68.00));
	}

}

