<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create('applicants', function(Blueprint $table)
		{
			$table->integer('studentNumber');
			$table->integer('m101');
			$table->integer('m102');
			$table->integer('m103');
			$table->decimal('firstYearAverage');
			$table->integer('m201');
			$table->integer('m202');
			$table->integer('m203');
			$table->decimal('secondYearAverage');
			$table->integer('m301');
			$table->integer('m302');
			$table->integer('m303');
			$table->decimal('thirdYearAverage');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applicants');
	}

}
