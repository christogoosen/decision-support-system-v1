<?php


//Route::get('/dss', array('as' => 'upload', 'uses' => 'HomeController@fileupload'));

Route::get('/', array('as' => 'upload', 'uses' => 'HomeController@getUpload'));

Route::get('/test', array('as' => 'seanGraph', 'uses' => 'seantestController@fetchTestData'));

Route::get('/graph', array('as' => 'testgraphData', 'uses' => 'GraphController@graph1'));

Route::get('/gtest', function(){
	return View::make("graphTest");
});

Route::any('upload-zip', function(){
 //return Input::file('file')->move(__DIR__.'',Input::file('file')->getClientOriginalName()); -->original upload
 //return Input::file('file')->move('uploads',Input::file('file')->getClientOriginalName());
if(Input::hasFile('file')) {
$upload_success = Input::file('file')->move('uploads',Input::file('file')->getClientOriginalName());
}
else{Session::flash('message', 'No file has been specified');
	return View::make('upload');}


if( $upload_success ) {
return FileController::unzip($upload_success);
//return Route::get('uploadSuccessfull', array('as' => 'uploadSuccess', 'uses' => 'FileController'));
//return Response::json('success', 200);
} 
else {
   return Response::json('error', 400);
}

});

