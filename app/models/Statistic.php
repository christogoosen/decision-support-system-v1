<?php

class Statistic extends Eloquent {

        protected $table = 'statistics';
        protected $fillable = array('module', 'year', 'moduleAverage', 'applicantAverage', 'standardDeviation');

}