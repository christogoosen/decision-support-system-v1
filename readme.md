Project description:
HonsBA (Socio-Informatics)
INTEGRATED ASSESSMENT 2014



Background

For this assessment you are expected to develop a decision-support system which visualises students’ academic results to aid decision makers responsible for postgraduate admissions. The development of your system must be based on the following, hypothetical case:



Case

A university offers a single undergraduate and a single postgraduate programme in the area of Informatics. The undergraduate programme spans three years and in each year students complete three modules – one relating to Organisations, one relating to Technology, and one relating to Sociology. To gain entry into the postgraduate programme students must complete all the relevant modules offered in the undergraduate programme. Unfortunately only 15 students can be accommodated in the postgraduate programme while between 20 and 70 students apply for it every year.

At this time a small committee of staff decides which applications to accept and which to reject. They consider the following factors when making their decisions:

1.  The student must have passed all the modules in the undergraduate programme to be accepted for the postgraduate programme. They must also have passed these modules within the past three years, and have passed them the first time they attempted them.

2.  Students’ general performance across all modules is seen as a good indicator of their readiness for the postgraduate programme; however, in the past students that have performed poorly (<60%) in the Technical modules have experienced difficulty in the postgraduate programme. Hence, the committee takes special interest in the results of these modules across the three years.

3.  The  committee  adopts  the  standpoint  that  students  mature  over  their  three  years  of undergraduate study. Hence, they place greater emphasis on the results of higher levels (i.e. year 3 is more important than year 2, which is more important than year 1).

In recent years the committee has realised that they have not always been consistent and rational in their application of the selection criteria and wish to improve upon this. More importantly, the number of applications has been growing steadily making the selection process time-consuming and difficult.

They have, consequently, considered the development of a decision-support system to assist them. Importantly, They do not wish to automate the process entirely since they value their ability to make judgments based on their experience but they would like to have some form of support system which enables them to graphically compare the applicants based on their academic performance.



Project Description

For this project your group is expected to address the committee’s requirements by developing a web- based data visualisation application to support the selection process. You may use any development stack that you feel is appropriate to achieve this requirement.
As part of this assessment you will receive an example dataset consisting of a ZIP archive containing 10
CSV files:

•  Nine files containing the results of the 9 undergraduate modules completed over the last three years. Each file contains rows with one student number and the final result for the module per row. There is a header row. Each file is named as follow l0t_yyyy.csv where

•  l represents the level of the module. 1, 2 or 3 for 1st - 3rd year.

•  t represents the type of the module. 1 for organisational; 2 for sociological and 3 for technical.

•  yyyy represents the year in which the module was completed.

•  other characters are literals within the file name.

•  One file containing the list of applicant student numbers named applicants.csv

The data is generated randomly and any resemblance to actual modules and students is purely coincidental.

The purpose of your application must be to visualise the data in a way that supports the committee’s decision  making  efforts  by  managing  their  cognitive  load  and  ensuring  that  the  criteria  is  applied correctly.



Assessment

•	The project must be completed in preselected groups of four. You will find the groups at the end of this brief.

•	On Friday 14 November between 9:00 and 11:00 you will be given an opportunity to facilitate the selection process for 2015 using your application. At the start of this session you will be given a new dataset (in the exact same format as your example dataset). For the purpose of your facilitation session the new dataset must be uploaded to your system as a single file in .zip format.you should upload the archive you are provided with directly to your system. IE: You must upload the ZIP file, rather than each file individually.

•	You will have a maximum of 30 minutes to perform the facilitation session. Your facilitation session will be brought to a halt at 30 minutes.

•    You should also submit a ZIP archive of your system to SUNLearn by 9:00.

•    A general impression mark will be awarded to each group.

•    Following the session you will complete a form in which you will be asked to evaluate your peers.

•    The results of the peer evaluation will be used to determine individual marks.


## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
